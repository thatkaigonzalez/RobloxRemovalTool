package main

import (
	"log"
  "fmt"
	"os"
	"os/user"
  "path/filepath"
	"strings"
)

func get_home_directory() string {
  usr, _ := user.Current()
  dir := usr.HomeDir
  return dir
}

func FilePathWalkDir(root string) ([]string, error) {
    var files []string
    err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
        if !info.IsDir() {
            files = append(files, path)
        }
        return nil
    })
    return files, err
}

func main() {
  var ff int
  var file_count int
  var files_removed int

  log.SetOutput(os.Stdout)

  log.Println("User home dir: ", get_home_directory())

  log.Println("Listing files in directory..");

  files, err := FilePathWalkDir(get_home_directory())
  
  if err != nil {
    log.Println("Error reading dir")
  }

  for _, f := range files {
    has := strings.Contains(f, "roblox")
    if has == true {
      ff = 1
      file_count += 1
      log.Println("found a file that contains the word 'roblox': ", f)
      log.Println("Should I remove it?")

      var yn string 
      fmt.Scanln(&yn)

      if yn == "n" {
        log.Println("OK. Operation aborted.")
      } else {
        os.Remove(f)
        files_removed += 1
      }

    }
  }

  if ff == 0 {
    log.Println("ZERO Files containing 'roblox' were found.")
  } else {
    if files_removed > 0 {
      log.Println("I found", file_count, "files containing 'roblox' and removed them.")
    } else {
      log.Println("I found",file_count,"but you told me not to remove them.")
    }
  }
}
